﻿using System;


namespace BSA2021_ProjectStructure.PL.Models
{
    public class NewTaskModel
    {
        public int ProjectId { get; set; }
        public int? PerformerId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public TaskStateModel State { get; set; } = TaskStateModel.Unfinished;
        public DateTime CreatedAt { get; set; } = DateTime.Now;
    }
}
