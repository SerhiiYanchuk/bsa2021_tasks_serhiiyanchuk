﻿

namespace BSA2021_ProjectStructure.PL.Models.QueryResultModels
{
    public class FinishedTaskModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
