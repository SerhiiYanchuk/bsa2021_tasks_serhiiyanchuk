﻿using BSA2021_ProjectStructure.PL.Interfaces;
using BSA2021_ProjectStructure.PL.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Json;
using System.Threading.Tasks;
using Timer = System.Timers.Timer;

namespace BSA2021_ProjectStructure.PL.Services
{
    public class TaskCompletingService : ITaskCompletingService
    {
        private readonly Timer _timer = new Timer();
        private readonly HttpClient _client = new HttpClient();
        private readonly string _routeToApiTask;
        public TaskCompletingService(string routeToApiTask)
        {
            _routeToApiTask = routeToApiTask;
            _timer.Elapsed += async (sender, args) =>
            {
                try
                {
                    var markedTaskId = await MarkRandomTaskWithDelay();
                    // Console.WriteLine($"[From TaskCompletingService] Task ID {markedTaskId} completed");
                }
                catch (Exception ex)
                {
                    // Console.WriteLine($"[From TaskCompletingService] {ex.Message}");
                }
                
            };
        }
        public void Start(int timeoutMs)
        {
            _timer.Stop();
            _timer.Interval = timeoutMs;
            _timer.AutoReset = true;
            _timer.Start();
        }
        public void Stop()
        {
            _timer.Stop();
        }
        private Task<int> MarkRandomTaskWithDelay()
        {
            TaskCompletionSource<int> tcs = new TaskCompletionSource<int>();
            Task.Run(async () =>
            {
                // get tasks
                var response = await _client.GetAsync(_routeToApiTask);
                if (!response.IsSuccessStatusCode)
                {
                    tcs.SetException(new Exception($"Status: {response.StatusCode}, {response.Content}"));
                }

                // filter unfinished tasks
                var tasks = JsonConvert.DeserializeObject<IEnumerable<TaskModel>>(await response.Content.ReadAsStringAsync()).Where(t => !t.FinishedAt.HasValue);
                if (!tasks.Any())
                    tcs.SetException(new Exception($"All tasks are completed")); ;

                // set finished random task
                var randomTask = tasks.ElementAt(new Random().Next(0, tasks.Count()));
                randomTask.FinishedAt = DateTime.Now;
                randomTask.State = TaskStateModel.Finished;

                // update task
                response = await _client.PutAsJsonAsync(_routeToApiTask, randomTask);
                if (!response.IsSuccessStatusCode)
                {
                    tcs.SetException(new Exception($"Status: {response.StatusCode}, {response.Content}"));
                }
                tcs.SetResult(randomTask.Id);
            });          
            return tcs.Task;
        }
    }
}
