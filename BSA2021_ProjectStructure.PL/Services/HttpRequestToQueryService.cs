﻿using BSA2021_ProjectStructure.PL.Interfaces;
using BSA2021_ProjectStructure.PL.Models;
using BSA2021_ProjectStructure.PL.Models.QueryResultModels;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace BSA2021_ProjectStructure.PL.Services
{
    public class HttpRequestToQueryService: IHttpRequestToQueryService
    {
        private readonly string _serverURL;
        private readonly HttpClient _client = new HttpClient();
        public HttpRequestToQueryService(string serverURL)
        {
            _serverURL = serverURL ?? "https://localhost:44361";
            _client = new HttpClient();
            _client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        }
        private async Task<HttpResponseMessage> GetData(string route)
        {
            var response = await _client.GetAsync(_serverURL + route);
            if (!response.IsSuccessStatusCode)
            {
                Console.WriteLine($"Status: {response.StatusCode}, {response.Content}");
                return null;
            }
            return response;
        }
        public async Task<IDictionary<ProjectModel, int>> GetUserProjectsWithQuantityTask(int performerId)
        {
            var response = await GetData($"/api/users/{performerId}/projects");
            if (response is not null)
            {
                string serializedArray = await response.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<IEnumerable<KeyValuePair<ProjectModel, int>>>(serializedArray).ToDictionary(p => p.Key, p => p.Value);
            }
            return new Dictionary<ProjectModel, int>();
        }

        public async Task<IEnumerable<TaskModel>> GetUserTasks(int performerId)
        {
            var response = await GetData($"/api/users/{performerId}/tasks");
            if (response is not null)
                return JsonConvert.DeserializeObject<IEnumerable<TaskModel>>(await response.Content.ReadAsStringAsync());
            return new List<TaskModel>();       
        }

        public async Task<IEnumerable<FinishedTaskModel>> GetFinishedTaskInCurrentYear(int performerId)
        {
            var response = await GetData($"/api/users/{performerId}/tasks/finished");
            if (response is not null)
                return JsonConvert.DeserializeObject<IEnumerable<FinishedTaskModel>>(await response.Content.ReadAsStringAsync());
            return new List<FinishedTaskModel>();        
        }

        public async Task<IEnumerable<TeamShortInfoModel>> GetTeamShortInfo()
        {
            var response = await GetData($"/api/teams/info");
            if (response is not null)
                return JsonConvert.DeserializeObject<IEnumerable<TeamShortInfoModel>>(await response.Content.ReadAsStringAsync());
            return new List<TeamShortInfoModel>();
        }

        public async Task<IEnumerable<PerformerWithTasksModel>> GetUsersWihtTasks()
        {
            var response = await GetData($"/api/users/tasks");
            if (response is not null)
                return JsonConvert.DeserializeObject<IEnumerable<PerformerWithTasksModel>>(await response.Content.ReadAsStringAsync());
            return new List<PerformerWithTasksModel>();
        }

        public async Task<UserDetailModel> GetUserDetail(int userId)
        {
            var response = await GetData($"/api/users/{userId}/detail");
            if (response is not null)
                return JsonConvert.DeserializeObject<UserDetailModel>(await response.Content.ReadAsStringAsync());
            return null;
        }

        public async Task<IEnumerable<ProjectDetailModel>> GetProjectsDetail()
        {
            var response = await GetData($"/api/projects/detail");
            if (response is not null)
                return JsonConvert.DeserializeObject<IEnumerable<ProjectDetailModel>>(await response.Content.ReadAsStringAsync());
            return new List<ProjectDetailModel>();
        }
      
        public void Dispose()
        {
            _client?.Dispose();
        }
    }
}
