﻿using BSA2021_ProjectStructure.PL.Interfaces;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.Http.Json;
using System.Threading.Tasks;

namespace BSA2021_ProjectStructure.PL.Services
{
    public class CrudService<TEntity, TNewEntity> : ICrudService<TEntity, TNewEntity> where TEntity : class
                                                                                      where TNewEntity : class
    {
        private readonly string _routeToEnitity;
        private readonly HttpClient _client = new HttpClient();

        public IModelFactory<TEntity, TNewEntity> ModelFactory { get; }

        public CrudService(string routeToEntity, IModelFactory<TEntity, TNewEntity> modelFactory)
        {
            _routeToEnitity = routeToEntity;
            ModelFactory = modelFactory;
            _client = new HttpClient();
            _client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        }

        public async Task<IEnumerable<TEntity>> GetAll()
        {
            var response = await GetData();
            if (response is not null)
                return JsonConvert.DeserializeObject<IEnumerable<TEntity>>(await response.Content.ReadAsStringAsync());
            return null;           
        }
        public async Task<TEntity> FindById(int id)
        {
            var response = await GetData(id);
            if (response is not null)
                return JsonConvert.DeserializeObject<TEntity>(await response.Content.ReadAsStringAsync());
            return null;
        }
        public async Task<TEntity> Insert(TNewEntity item)
        {
            var response = await PostData(item);
            if (response is not null)
                return JsonConvert.DeserializeObject<TEntity>(await response.Content.ReadAsStringAsync());
            return null;
        }
        public async Task Update(TEntity item)
        {
            await PutData(item);
        }
        public async Task Delete(int id)
        {
            await DeleteData(id);
        }       
        
        private async Task<HttpResponseMessage> GetData(int? id = null)
        {
            var response = await _client.GetAsync(_routeToEnitity + "/" + id ?? "");
            if (!response.IsSuccessStatusCode)
            {
                Console.WriteLine($"Status: {response.StatusCode}, {response.Content}");
                return null;
            }
            return response;
        }
        private async Task<HttpResponseMessage> PostData(TNewEntity newEntity)
        {
            var response = await _client.PostAsJsonAsync(_routeToEnitity, newEntity);
            if (!response.IsSuccessStatusCode)
            {
                Console.WriteLine($"Status: {response.StatusCode}, {response.Content}");
                return null;
            }
            return response;
        }
        private async Task<HttpResponseMessage> PutData(TEntity entity)
        {
            var response = await _client.PutAsJsonAsync(_routeToEnitity, entity);
            if (!response.IsSuccessStatusCode)
            {
                Console.WriteLine($"Status: {response.StatusCode}, {response.Content}");
                return null;
            }
            return response;
        }
        private async Task<HttpResponseMessage> DeleteData(int id)
        {
            var response = await _client.DeleteAsync(_routeToEnitity + "/" + id);
            if (!response.IsSuccessStatusCode)
            {
                Console.WriteLine($"Status: {response.StatusCode}, {response.Content}");
                return null;
            }
            return response;
        }
    }
}
