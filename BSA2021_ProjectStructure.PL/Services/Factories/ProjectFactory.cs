﻿using BSA2021_ProjectStructure.PL.Interfaces;
using BSA2021_ProjectStructure.PL.Models;
using System;

namespace BSA2021_ProjectStructure.PL.Services.Factories
{
    public class ProjectFactory : IModelFactory<ProjectModel, NewProjectModel>
    {
        public NewProjectModel CreateModel()
        {
            int authorId;
            while (true)
            {
                Console.Write("Author ID > ");
                if (int.TryParse(Console.ReadLine(), out authorId))
                    break;
            }
            int teamId;
            while (true)
            {
                Console.Write("Team ID > ");
                if (int.TryParse(Console.ReadLine(), out teamId))
                    break;
            }
            Console.Write("Name > ");
            string name = Console.ReadLine();

            Console.Write("Description > ");
            string description = Console.ReadLine();

            int year;
            while (true)
            {
                Console.Write("Deadline year > ");
                if (int.TryParse(Console.ReadLine(), out year))
                    break;
            }
            int month;
            while (true)
            {
                Console.Write("Deadline month > ");
                if (int.TryParse(Console.ReadLine(), out month))
                    break;
            }
            int day;
            while (true)
            {
                Console.Write("Deadline day > ");
                if (int.TryParse(Console.ReadLine(), out day))
                    break;
            }
            DateTime deadline = new DateTime(year, month, day);
            return new NewProjectModel
            {
                AuthorId = authorId,
                TeamId = teamId,
                Name = name,
                Description = description,
                Deadline = deadline
            };
        }

        public void UpdateModel(ProjectModel entity)
        {
            Console.Write("Change author [y/n] > ");
            char key = Console.ReadKey().KeyChar;
            Console.WriteLine();
            if (key == 'y' || key == 'Y')
            {
                int authorId;
                while (true)
                {
                    Console.Write("Author ID > ");
                    if (int.TryParse(Console.ReadLine(), out authorId))
                        break;
                }
                entity.AuthorId = authorId;
            }

            Console.Write("Change team [y/n] > ");
            key = Console.ReadKey().KeyChar;
            Console.WriteLine();
            if (key == 'y' || key == 'Y')
            {
                int teamId;
                while (true)
                {
                    Console.Write("Team ID > ");
                    if (int.TryParse(Console.ReadLine(), out teamId))
                        break;
                }
                entity.TeamId = teamId;
            }

            Console.Write("Change name [y/n] > ");
            key = Console.ReadKey().KeyChar;
            Console.WriteLine();
            if (key == 'y' || key == 'Y')
            {
                Console.Write("Name > ");
                string name = Console.ReadLine();
                entity.Name = name;
            }

            Console.Write("Change description [y/n] > ");
            key = Console.ReadKey().KeyChar;
            Console.WriteLine();
            if (key == 'y' || key == 'Y')
            {
                Console.Write("Description > ");
                string description = Console.ReadLine();
                entity.Description = description;
            }

            Console.Write("Change deadline [y/n] > ");
            key = Console.ReadKey().KeyChar;
            Console.WriteLine();
            if (key == 'y' || key == 'Y')
            {
                int year;
                while (true)
                {
                    Console.Write("Deadline year > ");
                    if (int.TryParse(Console.ReadLine(), out year))
                        break;
                }
                int month;
                while (true)
                {
                    Console.Write("Deadline month > ");
                    if (int.TryParse(Console.ReadLine(), out month))
                        break;
                }
                int day;
                while (true)
                {
                    Console.Write("Deadline day > ");
                    if (int.TryParse(Console.ReadLine(), out day))
                        break;
                }
                DateTime deadline = new DateTime(year, month, day);
                entity.Deadline = deadline;
            }
        }
    }
}
