﻿using BSA2021_ProjectStructure.PL.Interfaces;
using BSA2021_ProjectStructure.PL.Models;
using System;

namespace BSA2021_ProjectStructure.PL.Services.Factories
{
    public class TaskFactory : IModelFactory<TaskModel, NewTaskModel>
    {
        public NewTaskModel CreateModel()
        {
            int projectId;
            while (true)
            {
                Console.Write("Project ID > ");
                if (int.TryParse(Console.ReadLine(), out projectId))
                    break;
            }
            int performerId;
            while (true)
            {
                Console.Write("Performer ID > ");
                if (int.TryParse(Console.ReadLine(), out performerId))
                    break;
            }
            Console.Write("Name > ");
            string name = Console.ReadLine();

            Console.Write("Description > ");
            string description = Console.ReadLine();


            return new NewTaskModel
            {
                ProjectId = projectId,
                PerformerId = performerId,
                Name = name,
                Description = description
            };

        }

        public void UpdateModel(TaskModel entity)
        {
            Console.Write("Change project [y/n] > ");
            char key = Console.ReadKey().KeyChar;
            Console.WriteLine();
            if (key == 'y' || key == 'Y')
            {
                int projectId;
                while (true)
                {
                    Console.Write("Author ID > ");
                    if (int.TryParse(Console.ReadLine(), out projectId))
                        break;
                }
                entity.ProjectId = projectId;
            }

            Console.Write("Change performer [y/n] > ");
            key = Console.ReadKey().KeyChar;
            Console.WriteLine();
            if (key == 'y' || key == 'Y')
            {
                int performerId;
                while (true)
                {
                    Console.Write("Team ID > ");
                    if (int.TryParse(Console.ReadLine(), out performerId))
                        break;
                }
                entity.PerformerId = performerId;
            }

            Console.Write("Change name [y/n] > ");
            key = Console.ReadKey().KeyChar;
            Console.WriteLine();
            if (key == 'y' || key == 'Y')
            {
                Console.Write("Name > ");
                string name = Console.ReadLine();
                entity.Name = name;
            }

            Console.Write("Change description [y/n] > ");
            key = Console.ReadKey().KeyChar;
            Console.WriteLine();
            if (key == 'y' || key == 'Y')
            {
                Console.Write("Description > ");
                string description = Console.ReadLine();
                entity.Description = description;
            }          
        }
    }
}
