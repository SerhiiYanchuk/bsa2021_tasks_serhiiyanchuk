﻿using BSA2021_ProjectStructure.PL.Interfaces;
using BSA2021_ProjectStructure.PL.Models;
using System;

namespace BSA2021_ProjectStructure.PL.Services.Factories
{
    public class UserFactory : IModelFactory<UserModel, NewUserModel>
    {
        public NewUserModel CreateModel()
        {
            int teamId;
            while (true)
            {
                Console.Write("Team ID > ");
                if (int.TryParse(Console.ReadLine(), out teamId))
                    break;
            }
            Console.Write("First name > ");
            string firstName = Console.ReadLine();

            Console.Write("Last name > ");
            string lastName = Console.ReadLine();

            Console.Write("Email > ");
            string email = Console.ReadLine();

            int year;
            while (true)
            {
                Console.Write("Birthday year > ");
                if (int.TryParse(Console.ReadLine(), out year))
                    break;
            }
            int month;
            while (true)
            {
                Console.Write("Birthday month > ");
                if (int.TryParse(Console.ReadLine(), out month))
                    break;
            }
            int day;
            while (true)
            {
                Console.Write("Birthday day > ");
                if (int.TryParse(Console.ReadLine(), out day))
                    break;
            }
            DateTime birthday = new DateTime(year, month, day);
            return new NewUserModel
            {
                TeamId = teamId,
                FirstName = firstName,
                LastName = lastName,
                Email = email,
                Birthday = birthday
            };
        }

        public void UpdateModel(UserModel entity)
        {
            Console.Write("Change team [y/n] > ");
            char key = Console.ReadKey().KeyChar;
            Console.WriteLine();
            if (key == 'y' || key == 'Y')
            {
                int teamId;
                while (true)
                {
                    Console.Write("Team ID > ");
                    if (int.TryParse(Console.ReadLine(), out teamId))
                        break;
                }
                entity.TeamId = teamId;
            }

            Console.Write("Change first name [y/n] > ");
            key = Console.ReadKey().KeyChar;
            Console.WriteLine();
            if (key == 'y' || key == 'Y')
            {
                Console.Write("First name > ");
                string firstName = Console.ReadLine();
                entity.FirstName = firstName;
            }

            Console.Write("Change last name [y/n] > ");
            key = Console.ReadKey().KeyChar;
            Console.WriteLine();
            if (key == 'y' || key == 'Y')
            {
                Console.Write("Last name > ");
                string lastName = Console.ReadLine();
                entity.LastName = lastName;
            }

            Console.Write("Change email [y/n] > ");
            key = Console.ReadKey().KeyChar;
            Console.WriteLine();
            if (key == 'y' || key == 'Y')
            {
                Console.Write("Email > ");
                string email = Console.ReadLine();
                entity.Email = email;
            }

            Console.Write("Change birthday [y/n] > ");         
            key = Console.ReadKey().KeyChar;
            Console.WriteLine();
            if (key == 'y' || key == 'Y')
            {
                int year;
                while (true)
                {
                    Console.Write("Birthday year > ");
                    if (int.TryParse(Console.ReadLine(), out year))
                        break;
                }
                int month;
                while (true)
                {
                    Console.Write("Birthday month > ");
                    if (int.TryParse(Console.ReadLine(), out month))
                        break;
                }
                int day;
                while (true)
                {
                    Console.Write("Birthday day > ");
                    if (int.TryParse(Console.ReadLine(), out day))
                        break;
                }
                DateTime birthday = new DateTime(year, month, day);
                entity.Birthday = birthday;
            }
        }
    }
}
