﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BSA2021_ProjectStructure.PL.Interfaces
{
    public interface IModelFactory<TEntity, TNewEntity>
    {
        TNewEntity CreateModel();
        void UpdateModel(TEntity entity);
    }
}
