﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace BSA2021_ProjectStructure.PL.Interfaces
{
    public interface ICrudService<TEntity, TNewEntity>
    {
        IModelFactory<TEntity, TNewEntity> ModelFactory { get; }
        Task<IEnumerable<TEntity>> GetAll();
        Task<TEntity> FindById(int id);
        Task<TEntity> Insert(TNewEntity item);
        Task Update(TEntity item);
        Task Delete(int id);
    }
}
