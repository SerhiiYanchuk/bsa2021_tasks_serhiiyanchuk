﻿using System.Threading.Tasks;

namespace BSA2021_ProjectStructure.PL.Interfaces
{
    public interface ITaskCompletingService
    {
        void Start(int timeoutMs);
        void Stop();
    }
}
