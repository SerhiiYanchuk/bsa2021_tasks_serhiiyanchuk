﻿using BSA2021_ProjectStructure.PL.Models;
using BSA2021_ProjectStructure.PL.Models.QueryResultModels;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BSA2021_ProjectStructure.PL.Interfaces
{
    public interface IHttpRequestToQueryService: IDisposable
    {
        Task<IDictionary<ProjectModel, int>> GetUserProjectsWithQuantityTask(int performerId);
        Task<IEnumerable<TaskModel>> GetUserTasks(int performerId);
        Task<IEnumerable<FinishedTaskModel>> GetFinishedTaskInCurrentYear(int performerId);
        Task<IEnumerable<TeamShortInfoModel>> GetTeamShortInfo();
        Task<IEnumerable<PerformerWithTasksModel>> GetUsersWihtTasks();
        Task<UserDetailModel> GetUserDetail(int userId);
        Task<IEnumerable<ProjectDetailModel>> GetProjectsDetail();
    }
}
