﻿using BSA2021_ProjectStructure.BLL.Interfaces;
using BSA2021_ProjectStructure.Common.DTO;
using BSA2021_ProjectStructure.Common.DTO.QueryResultDTO;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BSA2021_TeamStructure.WebAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class TeamsController : ControllerBase
    {
        private readonly ITeamService _teamService;

        public TeamsController(ITeamService teamService)
        {
            _teamService = teamService;
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<TeamDTO>>> GetTeams()
        {
            return Ok(await _teamService.GetAll());
        }
        [HttpGet("info")]
        public async Task<ActionResult<IEnumerable<TeamShortInfoDTO>>> GetTeamsShortInfo([FromServices] ILinqSelectionService linqSelectionService)
        {
            return Ok(await linqSelectionService.GetTeamShortInfo());
        }
        [HttpGet("{id}")]
        public async Task<ActionResult<TeamDTO>> GetTeam(int id)
        {
            var team = await _teamService.FindById(id);
            if (team is null)
                return NotFound("ID doesn't exist");
            return Ok(team);
        }

        [HttpPost]
        public async Task<ActionResult<TeamDTO>> PostTeam([FromBody] NewTeamDTO teamDTO)
        {
            var createdTeam = await _teamService.Insert(teamDTO);
            return Created("/api/teams/" + createdTeam.Id, createdTeam);
        }

        [HttpPut]
        public async Task<IActionResult> PutTeam([FromBody] TeamDTO teamDTO)
        {
            if (await _teamService.CheckAvailability(teamDTO.Id) == false)
                return NotFound("ID doesn't exist");

            await _teamService.Update(teamDTO);
            return NoContent();
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteTeam(int id)
        {
            if (await _teamService.CheckAvailability(id) == false)
                return NotFound("ID doesn't exist");

            await _teamService.Delete(id);
            return NoContent();
        }
    }
}
