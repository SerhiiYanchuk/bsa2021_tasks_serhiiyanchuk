﻿using BSA2021_ProjectStructure.DAL.Context;
using BSA2021_ProjectStructure.DAL.Interfaces;
using BSA2021_ProjectStructure.DAL.Repositories;
using System.Threading.Tasks;

namespace BSA2021_ProjectStructure.DAL
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly ProjectDbContext _context;
        private readonly IProjectRepository _projectRepository;
        private readonly ITaskRepository _taskRoomRepository;
        private readonly ITeamRepository _teamRepository;
        private readonly IUserRepository _userRepository;
        public UnitOfWork(ProjectDbContext context)
        {
            _context = context;
            _projectRepository = new ProjectRepository(context);
            _taskRoomRepository = new TaskRepository(context);
            _teamRepository = new TeamRepository(context);
            _userRepository = new UserRepository(context);
        }
        public IProjectRepository Projects
        {
            get
            {
                return _projectRepository;
            }
        }
        public ITaskRepository Tasks
        {
            get
            {
                return _taskRoomRepository;
            }
        }
        public ITeamRepository Teams
        {
            get
            {
                return _teamRepository;
            }
        }
        public IUserRepository Users
        {
            get
            {
                return _userRepository;
            }
        }
      
        public int SaveChanges()
        {
            return _context.SaveChanges();
        }
        public Task<int> SaveChangesAsync()
        {
            return _context.SaveChangesAsync();
        }
        public void Dispose()
        {
            _context.Dispose();
        } 
    }
}
