﻿using BSA2021_ProjectStructure.DAL.Entities.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace BSA2021_ProjectStructure.DAL.Interfaces
{
    public interface IRepository<TEntity> where TEntity : Entity
    {
        Task<IEnumerable<TEntity>> GetAll(bool isTracked = true);
        Task<TEntity> FindById(int id);
        Task<IEnumerable<TEntity>> Find(Expression<Func<TEntity, bool>> predicate, bool isTracked = true);
        Task Insert(TEntity item);
        void Update(TEntity item);
        Task Delete(int id);
        void Delete(TEntity item);
        Task<bool> CheckAvailability(int id);
        Task<IQueryable<TEntity>> GetQuery();
    }
}
