﻿using BSA2021_ProjectStructure.DAL.Entities.Abstract;
using System;
using System.Collections.Generic;


namespace BSA2021_ProjectStructure.DAL.Entities
{
    public class Team : Entity
    {
        public string Name { get; set; }
        public DateTime CreatedAt { get; set; }
        public List<User> Members { get; set; } = new List<User>();
        public List<Project> Projects { get; set; } = new List<Project>();
    }
}
