﻿using BSA2021_ProjectStructure.DAL.Context;
using BSA2021_ProjectStructure.DAL.Entities.Abstract;
using BSA2021_ProjectStructure.DAL.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace BSA2021_ProjectStructure.DAL.Repositories
{
    public abstract class Repository<TEntity> : IRepository<TEntity> where TEntity : Entity
    {
        protected readonly ProjectDbContext _context;
        public Repository(ProjectDbContext context)
        {
            _context = context;
        }
        public virtual async Task<IEnumerable<TEntity>> GetAll(bool isTracked = true)
        {
            IQueryable<TEntity> set = _context.Set<TEntity>();
            if (!isTracked)
                set.AsNoTracking();
            return await set.ToListAsync();
        }
        public virtual async Task<TEntity> FindById(int id)
        {
            return await _context.FindAsync<TEntity>(id);
        }
        public virtual async Task<IEnumerable<TEntity>> Find(Expression<Func<TEntity, bool>> predicate, bool isTracked = true)
        {
            IQueryable<TEntity> set = _context.Set<TEntity>().Where(predicate);
            if (!isTracked)
                set.AsNoTracking();
            return await set.ToListAsync();
        }
        public virtual async Task Insert(TEntity item)
        {
            await _context.AddAsync(item);
        }
        public virtual void Update(TEntity item)
        {
            _context.Update(item);
        }
        public virtual async Task Delete(int id)
        {
            TEntity item = await FindById(id);
            Delete(item);
        }
        public virtual void Delete(TEntity item)
        {
            if (_context.Entry(item).State == EntityState.Detached)
                _context.Attach(item);
            _context.Remove(item);
        }
        public virtual async Task<bool> CheckAvailability(int id)
        {
            return await _context.Set<TEntity>().AnyAsync(p => p.Id == id);
        }
        public virtual async Task<IQueryable<TEntity>> GetQuery()
        {
            return _context.Set<TEntity>();
        }       
    }
}
