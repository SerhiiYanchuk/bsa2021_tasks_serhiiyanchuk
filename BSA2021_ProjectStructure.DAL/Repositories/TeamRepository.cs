﻿using BSA2021_ProjectStructure.DAL.Context;
using BSA2021_ProjectStructure.DAL.Entities;
using BSA2021_ProjectStructure.DAL.Interfaces;
using Task = System.Threading.Tasks.Task;

namespace BSA2021_ProjectStructure.DAL.Repositories
{
    public class TeamRepository : Repository<Team>, ITeamRepository
    {
        public TeamRepository(ProjectDbContext context) : base(context)
        {
        }

        // you can implement specific functionality and override the base implementation of CRUD
        public async Task LoadMembers(Team team)
        {
            await _context.Entry(team).Collection(p => p.Members).LoadAsync();
        }
        public async Task LoadProjects(Team team)
        {
            await _context.Entry(team).Collection(p => p.Projects).LoadAsync();
        }
    }
}
