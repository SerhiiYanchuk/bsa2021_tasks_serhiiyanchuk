﻿using AutoMapper;
using BSA2021_ProjectStructure.BLL.Infrastructure;
using BSA2021_ProjectStructure.BLL.Interfaces;
using BSA2021_ProjectStructure.BLL.Services;
using BSA2021_ProjectStructure.Common.DTO;
using BSA2021_ProjectStructure.DAL.Entities;
using BSA2021_ProjectStructure.DAL.Interfaces;
using FakeItEasy;
using System;
using Xunit;

namespace BSA2021_ProjectStructure.Tests
{
    public class TaskServiceTests
    {
        private readonly ITaskService _taskService;
        private readonly IUnitOfWork _unitOfWork;
        public TaskServiceTests()
        {
            _unitOfWork = A.Fake<IUnitOfWork>();
            var mapper = new MapperConfiguration(cfg => {
                cfg.AddProfile<ProjectMapperProfile>();
            }).CreateMapper();
            _taskService = new TaskService(_unitOfWork, mapper);
        }
        [Fact]
        public async System.Threading.Tasks.Task Update_WhenSetTaskAsFinishedAndUpdate_ThenUpdateHappened()
        {
            // arrange 
            TaskDTO editTask = new TaskDTO { Id = 1, State = TaskStateDTO.Finished, FinishedAt = DateTime.Now};
            // act
            await _taskService.Update(editTask);
            // assert    
            A.CallTo(() => _unitOfWork.SaveChangesAsync()).MustHaveHappened();
            A.CallTo(() => _unitOfWork.Tasks.Update(A<Task>.Ignored)).MustHaveHappened();
        }
    }
}
