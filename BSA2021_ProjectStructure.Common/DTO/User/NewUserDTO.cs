﻿using System;


namespace BSA2021_ProjectStructure.Common.DTO
{
    public class NewUserDTO
    {
        public int? TeamId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public DateTime RegisteredAt { get; set; } = DateTime.Now;
        public DateTime Birthday { get; set; }
    }
}


