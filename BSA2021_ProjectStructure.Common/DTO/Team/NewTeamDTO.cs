﻿using System;
using System.Collections.Generic;


namespace BSA2021_ProjectStructure.Common.DTO
{
    public class NewTeamDTO
    {
        public string Name { get; set; }
        public DateTime CreatedAt { get; set; } = DateTime.Now;
    }
}
