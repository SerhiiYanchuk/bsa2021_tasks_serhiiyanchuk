﻿using BSA2021_ProjectStructure.Common.DTO;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BSA2021_ProjectStructure.BLL.Interfaces
{
    public interface ITaskService
    {
        Task<IEnumerable<TaskDTO>> GetAll();
        Task<TaskDTO> FindById(int id);
        Task<TaskDTO> Insert(NewTaskDTO item);
        Task Update(TaskDTO item);
        Task Delete(int id);
        Task Delete(TaskDTO item);
        Task<bool> CheckAvailability(int id);
    }
}
