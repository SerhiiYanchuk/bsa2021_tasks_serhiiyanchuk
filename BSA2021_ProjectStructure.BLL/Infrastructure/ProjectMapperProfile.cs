﻿using AutoMapper;
using BSA2021_ProjectStructure.Common.DTO;
using BSA2021_ProjectStructure.DAL.Entities;

namespace BSA2021_ProjectStructure.BLL.Infrastructure
{
    public class ProjectMapperProfile : Profile
    {
        public ProjectMapperProfile()
        {
            CreateMap<Project, ProjectDTO>();
            CreateMap<Task, TaskDTO>();
            CreateMap<Team, TeamDTO>();
            CreateMap<User, UserDTO>().ReverseMap();

            CreateMap<ProjectDTO, Project>().ForMember(dest => dest.Author, src => src.Ignore())
                                            .ForMember(dest => dest.Team, src => src.Ignore())
                                            .ForMember(dest => dest.Tasks, src => src.Ignore());
            CreateMap<TaskDTO, Task>().ForMember(dest => dest.Performer, src => src.Ignore());
            CreateMap<TeamDTO, Team>().ForMember(dest => dest.Members, src => src.Ignore());

            CreateMap<NewProjectDTO, Project>();
            CreateMap<NewTaskDTO, Task>();
            CreateMap<NewTeamDTO, Team>();
            CreateMap<NewUserDTO, User>();
        }
    }
}