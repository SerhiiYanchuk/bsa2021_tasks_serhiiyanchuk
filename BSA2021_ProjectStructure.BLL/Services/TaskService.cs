﻿using AutoMapper;
using BSA2021_ProjectStructure.BLL.Interfaces;
using BSA2021_ProjectStructure.Common.DTO;
using BSA2021_ProjectStructure.DAL.Entities;
using BSA2021_ProjectStructure.DAL.Interfaces;
using System.Collections.Generic;

namespace BSA2021_ProjectStructure.BLL.Services
{
    public class TaskService : ITaskService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        public TaskService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }
        public async System.Threading.Tasks.Task<IEnumerable<TaskDTO>> GetAll()
        {
            return _mapper.Map<IEnumerable<TaskDTO>>(await _unitOfWork.Tasks.GetAll(false));
        }
        public async System.Threading.Tasks.Task<TaskDTO> FindById(int taskId)
        {
            return _mapper.Map<TaskDTO>(await _unitOfWork.Tasks.FindById(taskId));
        }
        public async System.Threading.Tasks.Task<TaskDTO> Insert(NewTaskDTO taskDTO)
        {
            var createdTask = _mapper.Map<Task>(taskDTO);
            await _unitOfWork.Tasks.Insert(createdTask);
            await _unitOfWork.SaveChangesAsync();
            return _mapper.Map<TaskDTO>(createdTask);
        }

        public async System.Threading.Tasks.Task Update(TaskDTO taskDTO)
        {
            Task editTask = await _unitOfWork.Tasks.FindById(taskDTO.Id);
            _mapper.Map<TaskDTO, Task>(taskDTO, editTask);
            _unitOfWork.Tasks.Update(editTask);
            await _unitOfWork.SaveChangesAsync();
        }

        public async System.Threading.Tasks.Task Delete(int taskId)
        {
            await _unitOfWork.Tasks.Delete(taskId);
            await _unitOfWork.SaveChangesAsync();
        }

        public async System.Threading.Tasks.Task Delete(TaskDTO taskDTO)
        {
            _unitOfWork.Tasks.Delete(_mapper.Map<Task>(taskDTO));
            await _unitOfWork.SaveChangesAsync();
        }
        public async System.Threading.Tasks.Task<bool> CheckAvailability(int taskId)
        {
            return await _unitOfWork.Tasks.CheckAvailability(taskId);
        }
       
    }
}
