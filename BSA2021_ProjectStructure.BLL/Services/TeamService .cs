﻿using AutoMapper;
using BSA2021_ProjectStructure.BLL.Interfaces;
using BSA2021_ProjectStructure.Common.DTO;
using BSA2021_ProjectStructure.DAL.Entities;
using BSA2021_ProjectStructure.DAL.Interfaces;
using System.Collections.Generic;

namespace BSA2021_ProjectStructure.BLL.Services
{
    public class TeamService : ITeamService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        public TeamService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }
        public async System.Threading.Tasks.Task<IEnumerable<TeamDTO>> GetAll()
        {
            return _mapper.Map<IEnumerable<TeamDTO>>(await _unitOfWork.Teams.GetAll(false));
        }
        public async System.Threading.Tasks.Task<TeamDTO> FindById(int teamId)
        {
            return _mapper.Map<TeamDTO>(await _unitOfWork.Teams.FindById(teamId));
        }
        public async System.Threading.Tasks.Task<TeamDTO> Insert(NewTeamDTO teamDTO)
        {
            var createdTeam = _mapper.Map<Team>(teamDTO);
            await  _unitOfWork.Teams.Insert(createdTeam);
            await _unitOfWork.SaveChangesAsync();
            return _mapper.Map<TeamDTO>(createdTeam);
        }

        public async System.Threading.Tasks.Task Update(TeamDTO teamDTO)
        {
            Team editTeam = await _unitOfWork.Teams.FindById(teamDTO.Id);
            _mapper.Map<TeamDTO, Team>(teamDTO, editTeam);
            _unitOfWork.Teams.Update(editTeam);
            await _unitOfWork.SaveChangesAsync();
        }

        public async System.Threading.Tasks.Task Delete(int teamId)
        {
            await _unitOfWork.Teams.Delete(teamId);
            await _unitOfWork.SaveChangesAsync();
        }

        public async System.Threading.Tasks.Task Delete(TeamDTO teamDTO)
        {
            _unitOfWork.Teams.Delete(_mapper.Map<Team>(teamDTO));
            await _unitOfWork.SaveChangesAsync();
        }
        public async System.Threading.Tasks.Task<bool> CheckAvailability(int teamId)
        {
            return await _unitOfWork.Teams.CheckAvailability(teamId);
        }
       
    }
}
