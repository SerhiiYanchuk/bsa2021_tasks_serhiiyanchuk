﻿using AutoMapper;
using BSA2021_ProjectStructure.BLL.Interfaces;
using BSA2021_ProjectStructure.Common.DTO;
using BSA2021_ProjectStructure.DAL.Entities;
using BSA2021_ProjectStructure.DAL.Interfaces;
using System.Collections.Generic;

namespace BSA2021_ProjectStructure.BLL.Services
{
    public class UserService : IUserService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        public UserService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }
        public async System.Threading.Tasks.Task<IEnumerable<UserDTO>> GetAll()
        {
            return _mapper.Map<IEnumerable<UserDTO>>(await _unitOfWork.Users.GetAll(false));
        }
        public async System.Threading.Tasks.Task<UserDTO> FindById(int userId)
        {
            return _mapper.Map<UserDTO>(await _unitOfWork.Users.FindById(userId));
        }
        public async System.Threading.Tasks.Task<UserDTO> Insert(NewUserDTO userDTO)
        {
            var createdUser = _mapper.Map<User>(userDTO);
            await _unitOfWork.Users.Insert(createdUser);
            await _unitOfWork.SaveChangesAsync();
            return _mapper.Map<UserDTO>(createdUser);
        }

        public async System.Threading.Tasks.Task Update(UserDTO userDTO)
        {
            User editUser = await _unitOfWork.Users.FindById(userDTO.Id);
            _mapper.Map<UserDTO, User>(userDTO, editUser);
            _unitOfWork.Users.Update(editUser);
            await _unitOfWork.SaveChangesAsync();
        }

        public async System.Threading.Tasks.Task Delete(int userId)
        {
            await _unitOfWork.Users.Delete(userId);
            await _unitOfWork.SaveChangesAsync();
        }

        public async System.Threading.Tasks.Task Delete(UserDTO userDTO)
        {
            _unitOfWork.Users.Delete(_mapper.Map<User>(userDTO));
            await _unitOfWork.SaveChangesAsync();
        }
        public async System.Threading.Tasks.Task<bool> CheckAvailability(int userId)
        {
            return await _unitOfWork.Users.CheckAvailability(userId);
        }

        public async System.Threading.Tasks.Task<IEnumerable<TaskDTO>> FindUserUnfinishedTasks(int performerId)
        {
            IEnumerable<Task> tasks = await _unitOfWork.Tasks.Find(t => t.PerformerId == performerId && t.FinishedAt == null);
            return _mapper.Map<IEnumerable<TaskDTO>>(tasks);
        }       
    }
}
