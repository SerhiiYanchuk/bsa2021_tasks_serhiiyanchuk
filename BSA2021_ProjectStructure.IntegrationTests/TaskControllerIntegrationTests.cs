using BSA2021_ProjectStructure.Common.DTO;
using BSA2021_ProjectStructure.DAL.Entities;
using BSA2021_ProjectStructure.WebAPI;
using Newtonsoft.Json;
using System;
using System.Net;
using System.Net.Http;
using System.Text;
using Xunit;

namespace BSA2021_ProjectStructure.IntegrationTests
{
    public class TaskControllerIntegrationTests: IClassFixture<CustomWebApplicationFactory<BSA2021_ProjectStructure.WebAPI.Startup>>
    {
        private readonly HttpClient _client;
        public TaskControllerIntegrationTests(CustomWebApplicationFactory<BSA2021_ProjectStructure.WebAPI.Startup> factory)
        {
            _client = factory.CreateClient();
        }
        private async System.Threading.Tasks.Task<int> PostTask(NewTaskDTO newTask)
        {
            string jsonInString = JsonConvert.SerializeObject(newTask);

            var httpResponse = await _client.PostAsync("api/tasks", new StringContent(jsonInString, Encoding.UTF8, "application/json"));
            var stringResponce = await httpResponse.Content.ReadAsStringAsync();
            if (httpResponse.StatusCode == HttpStatusCode.Created)
            {
                return JsonConvert.DeserializeObject<Task>(stringResponce).Id;
            }
            else
            {
                throw new InvalidOperationException($"Post request on route api/tasks has not responce 201. Status code: {(int)httpResponse.StatusCode}");
            }              
        }
        [Fact]
        public async System.Threading.Tasks.Task DeleteTask_WhenExistingTaskId_ThenResponseCode204AndGetByIdResponseCode404()
        {
            // act/ assert
            int generatedId = await PostTask(new NewTaskDTO());
            var httpResponse = await _client.DeleteAsync($"api/tasks/{generatedId}");
            Assert.Equal(HttpStatusCode.NoContent, httpResponse.StatusCode);

            var httpResponseGetById = await _client.GetAsync($"api/tasks/{generatedId}");
            Assert.Equal(HttpStatusCode.NotFound, httpResponseGetById.StatusCode);
        }
        [Fact]
        public async System.Threading.Tasks.Task DeleteTask_WhenNotExistingTaskId_ThenResponseCode404()
        {
            // act
            var httpResponse = await _client.DeleteAsync($"api/tasks/1");
            // assert
            Assert.Equal(HttpStatusCode.NotFound, httpResponse.StatusCode);
        }
    }
}
